//Added comment to check pushing from VS code

package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

// Own import
import java.util.Random;


public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        // TODO: Implement Rock Paper Scissors
        
        while (true) {
            
            System.out.println("Let's play round " + roundCounter);
            
            String humanChoice = userChoice();
            String computerChoice = randomChoice();

            String choiceString = "Human chose " +  humanChoice + ", computer chose " + computerChoice + ".";

            if(isWinner(humanChoice, computerChoice)) {
                System.out.println(choiceString + " Human wins.");
                humanScore++;
            } else if(isWinner(computerChoice, humanChoice)) {
                System.out.println(choiceString + " Computer wins.");
                computerScore++;
            } else {
                System.out.println(choiceString + " It`s a tie.");
            }

            System.out.println("Score: human " + humanScore + ", computer " + computerScore);

            char continueAnswer = continuePlaying();
            
            if (continueAnswer == 'n') {
                System.out.println("Bye bye :)");
                break;
            }

            roundCounter++;


        }
        

    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.nextLine();
        return userInput;
    }

    public String randomChoice() {
        Random rand = new Random();
        
        return rpsChoices.get(rand.nextInt(3));
    }

    public boolean isWinner(String choice1, String choice2) {
        if(choice1.equals("paper") ) {
            return choice2.equals("rock");
        } else if(choice1.equals("scissors")) {
            return choice2.equals("paper");
        } else {
            return choice2.equals("scissors");
        }
    }

    
    public boolean validateInput(String choice, List<String> choices) {
        return choices.contains(choice);
    }
    
    
    public String userChoice() {
        while(true) {
            String humanChoice = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();
            if (validateInput(humanChoice, rpsChoices)) {
                return humanChoice;
            } else {
                System.out.println("I don`t understand " + humanChoice + ". Try again");
            }
        }
    }

    public char continuePlaying() {
        while(true) {
            String continueAnswer = readInput("Do you wish to continue playing? (y/n)?").toLowerCase();
            if(validateInput(continueAnswer, Arrays.asList("y", "n"))) {
                return (char) continueAnswer.charAt(0);
            } else {
                System.out.println("I don`t understand " + continueAnswer + ". Try again");
            }
        }
    }

}
